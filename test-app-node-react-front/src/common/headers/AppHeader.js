import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Button from "@material-ui/core/Button";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  title: {
    flexGrow: 1,
  },
}));

const AppHeader = () => {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <AppBar position="static">
        <Toolbar>
          <Button className={classes.title} color="inherit" href={"/main"}>
            Main Page
          </Button>
          <Button className={classes.title} color="inherit" href={"/upload"}>
            Upload
          </Button>
        </Toolbar>
      </AppBar>
    </div>
  );
};

export default AppHeader;
