import React from "react";

const FileData = ({ selectedFile }) => {
  if (selectedFile) {
    return (
      <div>
        <h2>File Details:</h2>
        <p>File Name: {selectedFile.name}</p>
        <p>File Type: {selectedFile.type}</p>
      </div>
    );
  } else {
    return (
      <div>
        <br />
        <h4>Choose your video-file before Pressing the Upload button</h4>
      </div>
    );
  }
};

export default FileData;
