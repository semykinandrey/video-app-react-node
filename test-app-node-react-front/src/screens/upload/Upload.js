import { useState } from "react";
import { apiMethods } from "./../../services/api/api";
import FileData from "./components/FIleData";
import {
  Input,
  Button,
  Modal,
  Fade,
  Backdrop,
  makeStyles,
} from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    border: "2px solid #000",
    borderRadius: 10,
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
}));

const Upload = () => {
  const [selectedFile, setSelectedFile] = useState(null);
  const [open, setOpen] = useState(false);
  const [message, setMessage] = useState("");
  const classes = useStyles();

  const handleOpen = (responseMessage) => {
    setMessage(responseMessage);
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
    setMessage("");
  };

  const onFileChange = (event) => {
    setSelectedFile(event.target.files[0]);
  };

  const onFileUpload = async () => {
    const formData = new FormData();
    formData.append("file", selectedFile, selectedFile.name);
    const response = await apiMethods.postVideFile(formData);
    handleOpen(response.data);
  };

  return (
    <div>
      <h1>File upload</h1>
      <h3>using React and NodeJs!</h3>
      <div>
        <Input type="file" name="file" onChange={onFileChange} />
        <Button
          variant="contained"
          size="small"
          color="primary"
          onClick={onFileUpload}
          disabled={!selectedFile}
        >
          Upload!
        </Button>
      </div>
      <FileData selectedFile={selectedFile} />
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        className={classes.modal}
        open={open}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={open}>
          <div className={classes.paper}>
            <h2 id="transition-modal-title">{message}</h2>
            <div id="transition-modal-description"></div>
          </div>
        </Fade>
      </Modal>
    </div>
  );
};

export default Upload;
