import {
  Grid,
  Typography,
  Button,
  CardContent,
  CardActions,
  Card,
  makeStyles,
  Modal,
  Fade,
  Backdrop,
} from "@material-ui/core";
import { useState } from "react";
import VideoPlayer from "./VideoPlayer";

const useStyles = makeStyles((theme) => ({
  root: {
    minWidth: 200,
    minHeight: 200,
  },
  title: {
    fontSize: 14,
  },
  button: {
    margin: "auto",
  },
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    border: "2px solid #000",
    borderRadius: 10,
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
}));

const CardItem = ({ title }) => {
  const [open, setOpen] = useState(false);
  const classes = useStyles();

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const renderTitle = () => title.split(".")[0];

  return (
    <>
      <Grid item xs={12} sm={6} md={4} lg={3} xl={2}>
        <Card className={classes.root}>
          <CardContent>
            <Typography
              className={classes.title}
              color="textPrimary"
              gutterBottom
            >
              {renderTitle()}
            </Typography>
          </CardContent>
          <CardActions>
            <Button
              className={classes.button}
              variant="contained"
              size="small"
              color="primary"
              onClick={handleOpen}
            >
              Watch video
            </Button>
          </CardActions>
        </Card>
      </Grid>
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        className={classes.modal}
        open={open}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={open}>
          <div className={classes.paper}>
            <h2 id="transition-modal-title">{renderTitle()}</h2>
            <div id="transition-modal-description">
              <VideoPlayer videoName={title} />
            </div>
          </div>
        </Fade>
      </Modal>
    </>
  );
};

export default CardItem;
