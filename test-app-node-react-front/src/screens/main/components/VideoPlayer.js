import ReactPlayer from "react-player";

const VideoPlayer = ({ videoName }) => {
  return (
    <div>
      <ReactPlayer
        url={`http://localhost:65071/api/v1/video/${videoName}`}
        playing={true}
        controls={true}
      />
    </div>
  );
};

export default VideoPlayer;
