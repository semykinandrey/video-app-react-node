import { useEffect, useState } from "react";
import { apiMethods } from "../../services/api/api";
import { CircularProgress, Grid, makeStyles } from "@material-ui/core";
import CardItem from "./components/CardItem";

const useStyles = makeStyles((theme) => ({
  loader: {
    position: "absolute",
    height: "100%",
    left: "50%",
    top: "50%",
  },
  container: {
    margin: 40,
    paddingRight: "5%",
    width: "100%",
    height: "100%",
  },
}));

const Main = () => {
  const [videoList, setVideoList] = useState([]);
  const [isLoading, setIsLoading] = useState(true);

  const classes = useStyles();

  const renderList = () =>
    videoList.map((item, index) => <CardItem title={item} key={index} />);

  useEffect(() => {
    apiMethods
      .fetchVideoList()
      .then((res) => {
        console.log(res);
        setVideoList(res.data);
      })
      .catch((err) => console.warn(err))
      .finally(() => setIsLoading(false));
  }, []);

  if (isLoading) {
    return (
      <div className={classes.loader}>
        <CircularProgress />
      </div>
    );
  }
  return (
    <Grid className={classes.container} container spacing={4}>
      {renderList()}
    </Grid>
  );
};

export default Main;
