import "./App.css";
import { Switch, Redirect, BrowserRouter, Route } from "react-router-dom";
import Main from "./screens/main/Main";
import Upload from "./screens/upload/Upload";
import AppHeader from "./common/headers/AppHeader";

function App() {
  return (
    <div className="App">
      <AppHeader />
      <BrowserRouter>
        <Switch>
          <Route exact path="/" render={() => <Redirect to={"/main"} />} />
          <Route exact path="/main" render={() => <Main />} />
          <Route exact path="/upload" render={() => <Upload />} />
          <Route path="*" render={() => <div>404 PAGE NOT FOUND</div>} />
        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;
