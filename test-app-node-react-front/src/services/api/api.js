import axios from "axios";

const instance = axios.create({
  withCredentials: true,
  baseURL: "http://localhost:65071/api/v1/",
});

export const apiMethods = {
  async fetchVideoList() {
    return await instance.get("videos");
  },
  async postVideFile(formData) {
    return await instance.post("upload_video", formData);
  },
};
