import { handleBodyRequestParsing, handleCompression, handleCors, handleFileUpload } from './common';

export default [handleCors, handleBodyRequestParsing, handleCompression, handleFileUpload];
