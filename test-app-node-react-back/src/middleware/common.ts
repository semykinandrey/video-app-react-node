import { Router } from 'express';
import cors from 'cors';
import parser from 'body-parser';
import compression from 'compression';
import fileUpload from 'express-fileupload';

// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
export const handleCors = (router: Router) => router.use(cors({ credentials: true, origin: true }));

export const handleBodyRequestParsing = (router: Router): void => {
    router.use(parser.urlencoded({ extended: false }));
    router.use(parser.json());
};

export const handleCompression = (router: Router): void => {
    router.use(compression());
};

export const handleFileUpload = (router: Router): void => {
    router.use(fileUpload());
};
