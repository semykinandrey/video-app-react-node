import { NextFunction, Response } from 'express';
import { HTTP404Error, HTTPClientError } from '../utils/httpErrors';

export const notFoundError = (): void => {
    throw new HTTP404Error('Method not found.');
};

export const clientError = (err: Error, res: Response, next: NextFunction): void => {
    if (err instanceof HTTPClientError) {
        console.warn('Error: ', err);
        res.status(err.statusCode).send(err.message);
    } else {
        next(err);
    }
};

// eslint-disable-next-line @typescript-eslint/no-unused-vars
export const serverError = (err: Error, res: Response, next: NextFunction): void => {
    console.warn('Error: ', err);
    if (process.env.NODE_ENV === 'production') {
        res.status(500).send('Internal Server Error');
    } else {
        res.status(500).send(err.stack);
    }
};
