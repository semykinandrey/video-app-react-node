import http from 'http';
import express from 'express';
import { applyMiddleware, applyRoutes } from './utils';
import middleware from './middleware';
import errorHandlers from './middleware/errorHandlers';
import routes from './services';

const router = express();
applyMiddleware(middleware, router);
applyRoutes(routes, router);
applyMiddleware(errorHandlers, router);

process.on('uncaughtException', e => {
    console.error('Error uncaughtException: ', e);
    process.exit(1);
});

process.on('unhandledRejection', e => {
    console.error('Error unhandledRejection: ', e);
    process.exit(1);
});

const { PORT = 65071 } = process.env;
const server = http.createServer(router);

server.listen(PORT, () => console.info(`Server is runnig http://localhost:${PORT}`));
