import { Request, Response } from 'express';
import path from 'path';
import fs from 'fs';

export default [
    {
        path: '/api/v1/upload_video',
        method: 'post',
        handler: [
            async (req: Request, res: Response): Promise<void> => {
                console.log('<----------upload_video', req.files);
                if (!req.files || Object.keys(req.files).length === 0) {
                    console.error(`<<<<<<<upload_video_error`);
                    res.status(400).send('No files were uploaded.');
                } else {
                    const receivedFile = req.files.file;
                    console.log(`<<<<<< receivedFile ${receivedFile}`);
                    const directoryPath = path.join(__dirname, './../../../public/videos');
                    receivedFile.mv(`${directoryPath}/${receivedFile.name}`, err => {
                        if (err) {
                            console.log('<========err', err);
                            return res.status(500).send(err);
                        }
                        console.log(`<<<<<< Response File uploaded!`);
                        res.send('File uploaded!');
                    });
                }
            },
        ],
    },
    {
        path: '/api/v1/videos',
        method: 'get',
        handler: [
            async (req: Request, res: Response): Promise<void> => {
                console.info(`<<<<<< Response ${req}`);
                const directoryPath = path.join(__dirname, './../../../public/videos');
                console.log('<----------directoryPath', directoryPath);
                const result = fs.readdirSync(directoryPath);
                console.log('<--------result', result);
                res.send(result);
            },
        ],
    },
    {
        path: '/api/v1/video/:name',
        method: 'get',
        handler: [
            async (req: Request, res: Response): Promise<void> => {
                console.log('<----------video/:name', req.params.name);
                const filePath = path.join(__dirname, `./../../../public/videos/${req.params.name}`);
                console.log('<----------filePath', filePath);
                res.sendFile(filePath, err => {
                    if (err) {
                        console.error('<----------video/:name', err);
                        res.status(400).send('No such file were founded.');
                    }
                });
            },
        ],
    },
];
